#include "Board.h"
#include "Utilities.h"

Board::Board(int w, int h)
{
	width = w + 1;
	height = h;
	board = new char[ height * width + 1 ];
	for( int i = 0; i < width-1; i++ )
	{
		board[ i ] = BOARDER_SYMBOL;
	}
	for( int i = width; i < ( height - 1 )*width; i++ )
	{
		if( i % width == 0 || i % width == width -2 )
		{
			board[ i ] = BOARDER_SYMBOL;
		}
		else
		{
			board[ i ] = EMPTY_SYMBOL;
		}

	}
	for( int i = ( height - 1 ) * width; i < height * width; i++ )
	{
		board[ i ] = BOARDER_SYMBOL;
	}

	for( int i = 0; i < height; i++ )
	{
		board[ ( i + 1 )*( width )-1 ] = '\n';
	}
	board[ height * width ] = 0;
}

void Board::PrintBoard()
{
	std::cout << board;
}

bool Board::SetTile( unsigned char x , unsigned char y , char value )
{
	if( x > width - 3 || y > height - 3 ) // nobody should touch borders
	{
		return false;
	}
	else
	{
		board[ x + 1 + width * ( y + 1 ) ] = value;
		return true;
	}
}

std::optional<char> Board::GetTile( int x , int y )
{
	if( x > width - 3 || y > height - 3 || x < 0 || y < 0)
	{
		return BOARDER_SYMBOL;
	}
	else
	{
		return  board[ x + 1 + width * ( y + 1 ) ];
	}
}


Board::~Board()
{
	delete[] board;
}
