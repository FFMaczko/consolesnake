#pragma once
#include <iostream>
#include <optional>

class Board
{
private: 
	int width , height;
	char* board;
public:
	Board(int w, int h);
	void PrintBoard();
	bool SetTile(unsigned char x, unsigned char y, char value);
	std::optional<char> GetTile(int x, int y);
	~Board();
};

