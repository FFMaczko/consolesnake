#include "GameMenager.h"
#include<thread>
#include "WindowHandler.h"
#include<windows.h>
#include"InputMenager.h"
#include <stdlib.h>
#include <time.h>

GameMenager::GameMenager()
{
	srand( time( NULL ) );
	WindowHandler::GetWindowHandler().Create();
	CreateGame();
}

GameMenager& GameMenager::GetMenager()
{
	static GameMenager menager;
	return menager;
}

void GameMenager::RunGame()
{
	using namespace std::chrono_literals;
	auto startTimepoint = std::chrono::high_resolution_clock::now();
	auto endTimepoint = std::chrono::high_resolution_clock::now();
	auto start = std::chrono::time_point_cast<std::chrono::microseconds>( startTimepoint ).time_since_epoch().count();
	auto end = std::chrono::time_point_cast<std::chrono::microseconds>( endTimepoint ).time_since_epoch().count();
	auto duration = end - start;
	while( running )
	{
		startTimepoint = std::chrono::high_resolution_clock::now();
		RunSecene();
		endTimepoint = std::chrono::high_resolution_clock::now();
		start = std::chrono::time_point_cast<std::chrono::microseconds>( startTimepoint ).time_since_epoch().count();
		end = std::chrono::time_point_cast<std::chrono::microseconds>( endTimepoint ).time_since_epoch().count();
		duration = (end - start) * 0.001f;
		std::this_thread::sleep_for( std::chrono::milliseconds(FRAME_DELAY - duration) );
	}
}

void GameMenager::PlaceFood()
{
	int w , h;
	do
	{
		w = rand() % ( WindowHandler::GetWindowHandler().GetWidth() - 2 );
		h = rand() % ( WindowHandler::GetWindowHandler().GetHeight() - 3 );
	} while( board->GetTile(w,h) != EMPTY_SYMBOL );
	board->SetTile( w , h , FOOD_SYMBOL );
}

void GameMenager::GameOver()
{
	running = false;
	WindowHandler::GetWindowHandler().GameOverWindow();

}

void GameMenager::RunSecene()
{
	snake->MakeMove( *board , InputMenager::GetState() );
	board->PrintBoard();
	if( !running )
	{
		std::cout << "GAME OVER" << std::endl;
		std::cout << "You have collected: " << snake->GetLenght() << " pieces of food" << std::endl;
		std::cout << "PRESS R TO RESTART OR Q TO QUIT" << std::endl;
		while( true )
		{
			if( GetKeyState( 'R' ) & 0x8000 )
			{
				Restart();
			}
			if( GetKeyState( 'Q' ) & 0x8000 )
			{
				WindowHandler::GetWindowHandler().ResetWindow();
				break;
			}
		}
		
	}

}

void GameMenager::Restart()
{
	delete board;
	delete snake;
	WindowHandler::GetWindowHandler().ResetWindow();
	CreateGame();
	InputMenager::Reset();
	RunGame();
}

void GameMenager::CreateGame()
{
	board = new Board( WindowHandler::GetWindowHandler().GetWidth() - 1 , WindowHandler::GetWindowHandler().GetHeight() - 1 );
	snake = new Snake();
	snake->Create( *board );
	PlaceFood();
	running = true;
}


GameMenager::~GameMenager()
{
	delete board;
	delete snake;
}
