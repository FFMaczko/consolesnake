#pragma once
#include "Board.h"
#include "Utilities.h"
#include "Snake.h"
class GameMenager
{
private:
	Snake* snake;
	Board* board;
	bool running;
private:
	GameMenager();
	void RunSecene();
	void Restart();
	void CreateGame();
public:
	static GameMenager & GetMenager();
	void RunGame();
	void PlaceFood();
	void GameOver();
	~GameMenager();
};

