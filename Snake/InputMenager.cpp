#include "InputMenager.h"
#include<chrono>
#include<thread>
#include <Windows.h>
#include<iostream>

InputState InputMenager::state = NO_INPUT;
bool InputMenager::running = true;

InputMenager::InputMenager()
{
	state = NO_INPUT;
}


void InputMenager::StopInput()
{
	running = false;
}

InputState InputMenager::GetState()
{
	return state;
}

void InputMenager::ReadInputs()
{
	while( running )
	{
		if( GetKeyState( 'A' ) & 0x8000 )
		{
			state = LEFT;
		}
		else if( GetKeyState( 'D' ) & 0x8000 )
		{
			state = RIGHT;
		}
		else if( GetKeyState( 'W' ) & 0x8000 )
		{
			state = UP;
		}
		else if( GetKeyState( 'S' ) & 0x8000 )
		{
			state = DOWN;
		}
		std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
	}
}

void InputMenager::Reset()
{
	state = NO_INPUT;
}



InputMenager::~InputMenager()
{
}
