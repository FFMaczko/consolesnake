#pragma once
#include"Utilities.h"

class InputMenager
{
private:
	InputMenager();
	static InputState state;
	static bool running;
public:
	static void StopInput();
	static InputState GetState();
	static void ReadInputs();
	static void Reset();
	~InputMenager();
};

