#include "GameMenager.h"
#include<thread>
#include"InputMenager.h"
#include<windows.h>

void Game()
{
	GameMenager& menager = GameMenager::GetMenager();
	menager.RunGame();
}
void Input()
{
	InputMenager::ReadInputs();
}

void PrepareWindow()
{
	HWND console = GetConsoleWindow();
	ShowScrollBar( console , SB_BOTH , false );
	SetWindowLong( console , GWL_STYLE , GetWindowLong( console , GWL_STYLE ) & ~WS_MAXIMIZEBOX & ~WS_SIZEBOX
				   & ~WS_SYSMENU );
	BlockInput( true );
	int width , height;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo( GetStdHandle( STD_OUTPUT_HANDLE ) , &csbi );
	width = csbi.srWindow.Right - csbi.srWindow.Left + 1;
	height = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
	COORD c;
	c.X = width;
	c.Y = height;
	SetConsoleScreenBufferSize( GetStdHandle( STD_OUTPUT_HANDLE ) , c );
}

int main()
{
	PrepareWindow();
	std::cout << "Press WSAD to control the snake - PRESS ENTER TO START" << std::endl;
	if( std::cin.get() != '\n') std::cin.ignore( INT_MAX , '\n' );
	std::thread input( Input );
	std::thread game( Game );
	game.join();
	InputMenager::StopInput();
	input.join();
	std::cout << "Game created by FM" << std::endl;
	std::cout << "Here are your inputs - PRESS ENTER TO EXIT" << std::endl;
	std::cin.get();
}