#include "Snake.h"
#include"GameMenager.h"


Snake::Snake()
{
	length = 0;
}

void Snake::Create( Board & board )
{
	
	for( int i = 0; i < STARTING_LENGTH; i++ )
	{
		body.push_front( { i,3 } );
		board.SetTile( i , 3 , SNAKE_SYMBOL );
	}
}

int Snake::GetLenght()
{
	return length;
}

void Snake::MakeMove( Board & board , InputState direction )
{
	if( ( lastState == RIGHT && direction != LEFT ) || ( lastState == LEFT && direction != RIGHT )
		|| ( lastState == UP && direction != DOWN ) || ( lastState == DOWN && direction != UP ) 
		|| ( lastState == NO_INPUT && direction != LEFT))
	{
		lastState = direction;
	}
	Move( board , lastState );
}


void Snake::Move(Board& board, InputState direction )
{
	Vector2 newPosition = body.front();
	if( direction == RIGHT )
	{
		newPosition.x += 1;
	}
	if( direction == LEFT )
	{
		newPosition.x -= 1;
	}
	if( direction == DOWN )
	{
		newPosition.y += 1;
	}
	if( direction == UP )
	{
		newPosition.y -= 1;
	}
	if( direction != NO_INPUT )
	{
		
		if( board.GetTile( newPosition.x , newPosition.y ) == BOARDER_SYMBOL || board.GetTile( newPosition.x , newPosition.y ) == SNAKE_SYMBOL )
		{
			GameMenager::GetMenager().GameOver();
		}

		else if( board.GetTile( newPosition.x , newPosition.y ) == EMPTY_SYMBOL )
		{
			body.push_front( newPosition );
			board.SetTile( body.front().x , body.front().y , SNAKE_SYMBOL );
			board.SetTile( body.back().x , body.back().y , EMPTY_SYMBOL );
			body.pop_back();
		}

		else if( board.GetTile( newPosition.x , newPosition.y ) == FOOD_SYMBOL )
		{
			body.push_front( newPosition );
			board.SetTile( body.front().x , body.front().y , SNAKE_SYMBOL );
			length += 1;
			GameMenager::GetMenager().PlaceFood();
		}
	}
}



Snake::~Snake()
{
}
