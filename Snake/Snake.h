#pragma once
#include "Board.h"
#include"Utilities.h"
#include<list>

class Snake
{
private:
	InputState lastState;
	std::list<Vector2> body;
	int length;

private:
	void Move( Board& board , InputState direction );
public:
	Snake();
	void Create( Board& board);
	int GetLenght();
	void MakeMove( Board & board , InputState direction );
	~Snake();
};

