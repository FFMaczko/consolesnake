#pragma once
#include<iostream>
#include<chrono>
struct Timer
{
	std::chrono::time_point<std::chrono::high_resolution_clock> startTimepoint;
	Timer()
	{
		startTimepoint = std::chrono::high_resolution_clock::now();
	}
	~Timer()
	{
		auto endTimepoint = std::chrono::high_resolution_clock::now();
		auto start = std::chrono::time_point_cast<std::chrono::microseconds>( startTimepoint ).time_since_epoch().count();
		auto end = std::chrono::time_point_cast<std::chrono::microseconds>( endTimepoint ).time_since_epoch().count();
		auto duration = end - start;
		std::cout << duration * 0.001f << "ms" << std::endl;
	}
};

