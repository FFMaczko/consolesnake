#pragma once


#define SNAKE_SYMBOL 'x'
#define EMPTY_SYMBOL ' '
#define BOARDER_SYMBOL '#'
#define FOOD_SYMBOL '*'

#define STARTING_LENGTH 4

#define PERFECT_WIDTH  500
#define PERFECT_HEIGHT  600
#define FONT_SIZE 16

#define FRAME_DELAY 100
struct Vector2
{
	int x , y;

	bool operator ==( Vector2& other )
	{
		return x == other.x && y == other.y;
	}

	bool operator !=( Vector2& other )
	{
		return x != other.x || y != other.y;
	}
};

enum InputState
{
	UP , DOWN , LEFT , RIGHT , NO_INPUT
};