#include "WindowHandler.h"
#include "windows.h"
#include <iostream>


WindowHandler WindowHandler::window;

WindowHandler::WindowHandler()
{

}


WindowHandler& WindowHandler::GetWindowHandler()
{
	return window;
}

void WindowHandler::Create()
{
	SetConsoleTitle( "CHARly the snake" );

	//setting window size to 90% of desktop space
	RECT desktop;
	int width , height;
	const HWND hDesktop = GetDesktopWindow();
	GetWindowRect( hDesktop , &desktop );
	HWND console = GetConsoleWindow();
	MoveWindow( console , (desktop.right- desktop.left) /4  - PERFECT_WIDTH / 2 , desktop.bottom / 2 - PERFECT_HEIGHT / 2 , PERFECT_WIDTH 
				, PERFECT_HEIGHT , TRUE );
	//hiding scrool bars
	ShowScrollBar( console , SB_BOTH , false );
	//blocking input
	BlockInput( true );

	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof( cfi );
	cfi.nFont = 0;
	cfi.dwFontSize.X = FONT_SIZE;
	cfi.dwFontSize.Y = FONT_SIZE;
	cfi.FontFamily = FF_MODERN;
	cfi.FontWeight = FW_BOLD;
	SetCurrentConsoleFontEx( GetStdHandle( STD_OUTPUT_HANDLE ) , FALSE , &cfi );

	//setting font color to GREEN
	SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ) , FOREGROUND_GREEN );



	//checking window dimentions in chars
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo( GetStdHandle( STD_OUTPUT_HANDLE ) , &csbi );
	window.width = csbi.srWindow.Right - csbi.srWindow.Left + 1;
	window.height = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;

	COORD c;
	c.X = window.width;
	c.Y = window.height;

	SetConsoleScreenBufferSize( GetStdHandle( STD_OUTPUT_HANDLE ) , c );
	SetWindowLong( console , GWL_STYLE , GetWindowLong( console , GWL_STYLE ) & ~WS_MAXIMIZEBOX & ~WS_SIZEBOX 
					   & ~WS_SYSMENU);
		
}

void WindowHandler::ResetWindow()
{
	SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ) , FOREGROUND_GREEN );
}

void WindowHandler::GameOverWindow()
{
	SetConsoleTextAttribute( GetStdHandle( STD_OUTPUT_HANDLE ) , FOREGROUND_RED );
}

int WindowHandler::GetHeight()
{
	return height;
}

int WindowHandler::GetWidth()
{
	return width;
}

WindowHandler::~WindowHandler()
{
}
