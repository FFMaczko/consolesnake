#pragma once
#include"Utilities.h"

class WindowHandler
{
private:
	static WindowHandler window;
	int height , width;
private:
	WindowHandler();
public:
	static WindowHandler& GetWindowHandler();
	void Create();
	void ResetWindow();
	void GameOverWindow();
	int GetHeight();
	int GetWidth();
	~WindowHandler();
};

